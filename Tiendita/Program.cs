﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Tiendita.Models;

namespace Tiendita
{
    class Program
    {
        static void Main(string[] args)
        {
            Home();
        }

        public static void Home()
        {
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.WriteLine("Bienvenido al sistema Tiendita");
            Console.WriteLine("1) Iniciar sesión");
            Console.WriteLine("2) Crear cuenta");
            Console.WriteLine("0) Salir");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Digite el número de su elección:  ");
            string opcion = Console.ReadLine();
            try
            {
                switch (opcion)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("------------------------------------------------------------------------------");
                        Login();
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("------------------------------------------------------------------------------");
                        CrearUsuario();
                        break;
                    case "0":
                        Console.Clear();
                        return;
                }
                Console.Clear();
                Home();
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Home();
            }
        }

        #region Hashear y recuperar contrsenia
        /// <summary>
        /// Hashea la contraseña
        /// </summary>
        /// <param name="input"></param>
        /// <param name="algorithm"></param>
        /// <returns></returns>
        public static string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        /// <summary>
        /// Valida la contraseña ingresada
        /// </summary>
        /// <param name="input"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static bool verifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = ComputeHash(input, new MD5CryptoServiceProvider());

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Inicio de sesión y creacion de usuarios
        /// <summary>
        /// crea usuarios en la Bd
        /// </summary>
        public static void CrearUsuario()
        {
            try
            {
                Console.Write("Correo o nombre de usuario: ");
                string correo = Console.ReadLine();
                Console.Write("Contraseña: ");
                string pass = Console.ReadLine();
                Console.WriteLine("Guardando información...");
                string hPassword = ComputeHash(pass, new MD5CryptoServiceProvider());
                var user = new ApplicationUser
                {
                    UserName = correo,
                    Email = correo,
                    PasswordHash = hPassword,
                    EmailConfirmed = true
                };
                var usuario = new Usuario
                {
                    Correo = correo,
                    Password = hPassword,
                };
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    bool correoRepetido = context.Usuarios.Any(u => u.ApplicationUser.Email.Equals(correo));
                    if (!correoRepetido)
                    {
                        context.ApplicationUsers.Add(user);
                        usuario.ApplicationUser = user;
                        usuario.ApplicationUser.Id = user.Id;
                        context.Add(usuario);
                        context.SaveChanges();
                        Console.Clear();
                        Console.WriteLine("Usuario creado éxitosamente");
                        Home();
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("El correo o nombre de usuario ya esta registrado en el sistema.");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        CrearUsuario();

                    }
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Console.WriteLine("------------------------------------------------------------------------------");
                CrearUsuario();
            }

        }

        /// <summary>
        /// Inicia sesión al sistema
        /// </summary>
        public static void Login()
        {
            try
            {
                Console.Write("Correo o nombre de usuario: ");
                string correo = Console.ReadLine();
                Console.Write("Contraseña: ");
                string pass = Console.ReadLine();
                Console.WriteLine("Iniciando sesión...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    Usuario usuario = context.Usuarios.FirstOrDefault(u => u.Correo == correo);
                    if (usuario != null)
                    {
                        if (verifyMd5Hash(pass, usuario.Password))
                        {
                            Console.WriteLine("------------------------------------------------------------------------------");
                            Console.Clear();
                            Console.WriteLine("Sesión iniciada corectamente");
                            Menu(usuario.Correo);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Contraseña incorrecta, inténtelo de nuevo");
                            Console.WriteLine("------------------------------------------------------------------------------");
                            Login();
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No existe un usuario con el correo o nombre de usuario ingresado");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        Login();
                    }
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Console.WriteLine("------------------------------------------------------------------------------");
            }
        }
        #endregion

        public static void Menu(string NombreUsuario)
        {
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.WriteLine("Bienvenido " + NombreUsuario);
            Console.WriteLine("1) Modulo de productos");
            Console.WriteLine("2) Modulo de ventas");
            Console.WriteLine("3) Modulo de detalles");
            Console.WriteLine("0) Cerrar sesión");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Digite el número de su elección:  ");
            string opcion = Console.ReadLine();
            try
            {
                switch (opcion)
                {
                    case "1":
                        Console.Clear();
                        MenuProductos(NombreUsuario);
                        break;
                    case "2":
                        Console.Clear();
                        MenuVentas(NombreUsuario);
                        break;
                    case "3":
                        Console.Clear();
                        MenuDetalles(NombreUsuario);
                        break;
                    case "0":
                        Console.Clear();
                        Home();
                        break;
                }
                Console.Clear();
                Menu(NombreUsuario);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Menu(NombreUsuario);
            }
        }

        public static void MenuVentas(string NombreUsuario)
        {
            Console.WriteLine("Menu de ventas");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.WriteLine("1) Crear venta");
            Console.WriteLine("2) Buscar venta");
            Console.WriteLine("3) Actualizar venta");
            Console.WriteLine("4) Eliminar venta");
            Console.WriteLine("0) Regresar al menu anterior");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Digite el número de su elección:  ");
            string opcion = Console.ReadLine();
            try
            {
                switch (opcion)
                {
                    case "1":
                        Console.Clear();
                        CrearVenta(NombreUsuario);
                        break;
                    case "2":
                        Console.Clear();
                        BuscarVenta(NombreUsuario);
                        break;
                    case "3":
                        Console.Clear();
                        EditarVenta(NombreUsuario);
                        break;
                    case "4":
                        Console.Clear();
                        EliminarVenta(NombreUsuario);
                        break;
                    case "0":
                        Console.Clear();
                        Menu(NombreUsuario);
                        break;
                }
                Console.Clear();
                MenuVentas(NombreUsuario);
            }
            catch(Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuVentas(NombreUsuario);
            }
        }

        public static void MenuDetalles(string NombreUsuario)
        {
            Console.WriteLine("Menu de Detalles");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.WriteLine("1) Crear detalle");
            Console.WriteLine("2) Buscar detalle");
            Console.WriteLine("3) Actualizar detalle");
            Console.WriteLine("4) Eliminar detalle");
            Console.WriteLine("0) Regresar al menu anterior");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Digite el número de su elección:  ");
            string opcion = Console.ReadLine();
            try
            {
                switch (opcion)
                {
                    case "1":
                        Console.Clear();
                        CrearDetalle(NombreUsuario);
                        break;
                    case "2":
                        Console.Clear();
                        BuscarDetalle(NombreUsuario);
                        break;
                    case "3":
                        Console.Clear();
                        EditarDetalle(NombreUsuario);
                        break;
                    case "4":
                        Console.Clear();
                        EliminarDetalle(NombreUsuario);
                        break;
                    case "0":
                        Console.Clear();
                        Menu(NombreUsuario);
                        break;
                }
                Console.Clear();
                MenuDetalles(NombreUsuario);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuDetalles(NombreUsuario);
            }
        }

        public static void MenuProductos(string NombreUsuario)
        {
            Console.WriteLine("Menu de productos");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.WriteLine("1) Buscar producto");
            Console.WriteLine("2) Crear producto");
            Console.WriteLine("3) Actualizar producto");
            Console.WriteLine("4) Eliminar producto");
            Console.WriteLine("0) Regresar al menu anterior");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Digite el número de su elección:  ");
            string opcion = Console.ReadLine();
            try
            {
                switch (opcion)
                {
                    case "1":
                        Console.Clear();
                        BuscarProductos(NombreUsuario);
                        break;
                    case "2":
                        Console.Clear();
                        CrearProducto(NombreUsuario);
                        break;
                    case "3":
                        Console.Clear();
                        EditarProducto(NombreUsuario);
                        break;
                    case "4":
                        Console.Clear();
                        EliminarProducto(NombreUsuario);
                        break;
                    case "0":
                        Console.Clear();
                        Menu(NombreUsuario);
                        break;
                }
                Console.Clear();
                MenuProductos(NombreUsuario);
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuProductos(NombreUsuario);
            }
        }

        #region Metodos del modulo ventas
        public static void CrearVenta(string NombreUsuario)
        {
            Console.WriteLine("Crear venta");
            Console.WriteLine("------------------------------------------------------------------------------");
            Venta venta = new Venta();
            venta = LlenarVenta(venta);
            Console.WriteLine("Guardando información...");
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Add(venta);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Venta creada éxitosamente");
                    MenuVentas(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                CrearVenta(NombreUsuario);
            }
        }

        public static void BuscarVenta(string NombreUsuario)
        {
            Console.WriteLine("Buscar venta");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Ingrese el código de la venta o presione 'Enter' para mostrar todos los registros: ");
            //int Id = int.Parse(Console.ReadLine());
            string Id = Console.ReadLine();
            Console.WriteLine("Buscando...");
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var lista = context.Ventas.Where(v => v.Id.ToString().Contains(Id));
                    if (lista.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in lista)
                        {
                            Console.WriteLine("Código de la venta: " + item.Id + ", Total de la venta: " + item.Total + " MXN, Fecha de la venta: " + item.Fecha.ToShortDateString() + ", por el cliente: " + item.Cliente);
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No se encontraron registros...");
                    }
                    Console.WriteLine("------------------------------------------------------------------------------");
                    MenuVentas(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuVentas(NombreUsuario);
            }
        }

        public static void EditarVenta(string NombreUsuario)
        {
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var lista = context.Ventas.ToList();
                    if (lista.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in lista)
                        {
                            Console.WriteLine("Código de la venta: " + item.Id + ", Total de la venta: " + item.Total + " MXN, Fecha de la venta: " + item.Fecha.ToShortDateString() + ", por el cliente: " + item.Cliente);
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No se encontraron registros...");
                    }
                }
                Venta venta = SeleccionarVenta(NombreUsuario);
                venta = LlenarVenta(venta);
                Console.WriteLine("Actualizando registro...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Update(venta);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Venta actualizada éxitosamente");
                    MenuVentas(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuVentas(NombreUsuario);

            }

        }

        public static void EliminarVenta(string NombreUsuario)
        {
            Console.WriteLine("Eliminar venta");
            Console.WriteLine("------------------------------------------------------------------------------");
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var lista = context.Ventas.ToList();
                    if (lista.Any())
                    {
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in lista)
                        {
                            Console.WriteLine("Código de la venta: " + item.Id + ", Total de la venta: " + item.Total + " MXN, Fecha de la venta: " + item.Fecha.ToShortDateString() + ", Por el cliente: " + item.Cliente);
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No hay registros para eliminar...");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        MenuVentas(NombreUsuario);
                    }
                    Console.WriteLine("------------------------------------------------------------------------------");
                }
                Venta venta = SeleccionarVenta(NombreUsuario);
                Console.WriteLine("Eliminando registro...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Remove(venta);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Venta eliminada éxitosamente");
                    MenuVentas(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuVentas(NombreUsuario);
            }
        }

        public static Venta SeleccionarVenta(string NombreUsuario)
        {
            Console.Write("Seleciona el código de la venta: ");
            try
            {
                int Id = int.Parse(Console.ReadLine());
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    Venta venta = context.Ventas.Find(Id);
                    if (venta == null)
                    {
                        SeleccionarVenta(NombreUsuario);
                    }
                    return venta;
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Venta venta = new Venta();
                return venta;
            }
        }

        public static Venta LlenarVenta(Venta venta)
        {
            Console.Write("Monto total: ");
            venta.Total = decimal.Parse(Console.ReadLine());
            venta.Fecha = DateTime.Now;
            Console.Write("Nombre del cliente: ");
            venta.Cliente = Console.ReadLine();
            return venta;
        }
        #endregion

        #region Metodos del modulo Detalle

        public static void CrearDetalle(string NombreUsuario)
        {
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    Console.WriteLine("Validando datos...");
                    var listaProductos = context.Productos.ToList();
                    var listaVentas = context.Ventas.ToList();
                    if (listaProductos.Any() && listaVentas.Any())
                        Console.Clear();
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No hay productos y/o ventas registrados");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        MenuDetalles(NombreUsuario);
                    }
                }
                Console.WriteLine("Crear detalle");
                Console.WriteLine("------------------------------------------------------------------------------");
                Detalle detalle = new Detalle();
                detalle = LlenarDetalle(detalle, NombreUsuario);
                Console.WriteLine("Guardando información...");

                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Add(detalle);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Detalle creado éxitosamente");
                    MenuDetalles(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                CrearDetalle(NombreUsuario);
            }
        }

        public static Detalle LlenarDetalle(Detalle detalle, string NombreUsuario)
        {
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                Console.WriteLine("Buscando productos...");
                var listaProductos = context.Productos.ToList();
                Console.Clear();
                foreach (var item in listaProductos)
                {
                    Console.WriteLine("Código de producto: " + item.Id + ", Producto: " + item.Nombre + ", Precio : " + item.Precio + " MXN.");
                }
                Console.WriteLine("------------------------------------------------------------------------------");
            }
            Producto producto = SelecionarProducto();
            detalle.ProductoId = producto.Id;
            Console.Clear();
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                Console.WriteLine("Buscando ventas...");
                var listaVentas = context.Ventas.ToList();
                Console.Clear();
                foreach (var item in listaVentas)
                {
                    Console.WriteLine("Código de la venta: " + item.Id + ", Total de la venta: " + item.Total + " MXN, Fecha de la venta: " + item.Fecha.ToShortDateString() + ", Por el concepto de: " + item.Cliente);
                }
                Console.WriteLine("------------------------------------------------------------------------------");
            }
            Venta venta = SeleccionarVenta(NombreUsuario);
            detalle.VentaId = venta.Id;
            Console.Clear();
            Console.Write("Subtotal: ");
            detalle.Subtotal = decimal.Parse(Console.ReadLine());
            return detalle;
        }

        public static void BuscarDetalle(string NombreUsuario)
        {
            Console.WriteLine("Buscar detalle");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Ingrese el código del datelle o presione 'Enter' para mostrar todos los registros: ");
            string Id = Console.ReadLine();
            Console.WriteLine("Buscando...");
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var lista = context.Detalles
                        .Include(x => x.Producto)
                        .Include(x => x.Venta)
                        .Where(x => x.Id.ToString().Contains(Id));
                    if (lista.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in lista)
                        {
                            Console.WriteLine("Código del detalle: " + item.Id + ", La venta total fue de: " + item.Venta.Total + " MXN, Del producto: " + item.Producto.Nombre + ", El monto total del detalle es de: " + item.Subtotal + " MNX");
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No se encontraron registros...");
                    }
                    Console.WriteLine("------------------------------------------------------------------------------");
                    MenuDetalles(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuDetalles(NombreUsuario);
            }
        }

        public static void EditarDetalle(string NombreUsuario)
        {
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    Console.WriteLine("Validando datos...");
                    var listaProductos = context.Productos.ToList();
                    var listaVentas = context.Ventas.ToList();
                    var listaDetalle = context.Detalles.ToList();
                    if (listaDetalle.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in listaDetalle)
                        {
                            Console.WriteLine("Código del detalle: " + item.Id + ", La venta total fue de: " + item.Venta.Total + " MXN, Del producto: " + item.Producto.Nombre + ", El monto total del detalle es de: " + item.Subtotal + " MNX");
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No hay productos y/o ventas registrados");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        MenuDetalles(NombreUsuario);
                    }
                }
                Console.WriteLine("Editar detalle");
                Console.WriteLine("------------------------------------------------------------------------------");
                Detalle detalle = SeleccionarDetalle(NombreUsuario);
                detalle = LlenarDetalle(detalle, NombreUsuario);
                Console.WriteLine("Actualizando registro...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Update(detalle);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Detalle actualizado éxitosamente");
                    MenuDetalles(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuDetalles(NombreUsuario);
            }
        }

        public static Detalle SeleccionarDetalle(string NombreUsuario)
        {
            Console.Write("Seleciona el código del detalle: ");
            try
            {
                int Id = int.Parse(Console.ReadLine());
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    Detalle detalle = context.Detalles.Find(Id);
                    if (detalle == null)
                    {
                        SeleccionarDetalle(NombreUsuario);
                    }
                    return detalle;
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Detalle detalle = new Detalle();
                return detalle;
            }
        }

        public static void EliminarDetalle(string NombreUsuario)
        {
            Console.WriteLine("Eliminar detalle");
            Console.WriteLine("------------------------------------------------------------------------------");
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var lista = context.Detalles
                        .Include(x => x.Producto)
                        .Include(x => x.Venta)
                        .ToList();
                    if (lista.Any())
                    {
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in lista)
                        {
                            Console.WriteLine("Código del detalle: " + item.Id + ", La venta total fue de: " + item.Venta.Total + " MXN, Del producto: " + item.Producto.Nombre + ", El monto total del detalle es de: " + item.Subtotal + " MNX");
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No hay registros para eliminar...");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        MenuDetalles(NombreUsuario);
                    }
                    Console.WriteLine("------------------------------------------------------------------------------");
                }
                Detalle detalle = SeleccionarDetalle(NombreUsuario);
                Console.WriteLine("Eliminando registro...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Remove(detalle);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Detalle eliminado éxitosamente");
                    MenuDetalles(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuDetalles(NombreUsuario);
            }
        }

        #endregion

        #region Metodos del modulo Productos
        public static void CrearProducto(string NombreUsuario)
        {
            try
            {
                Console.WriteLine("Crear producto");
                Console.WriteLine("------------------------------------------------------------------------------");
                Producto producto = new Producto();
                producto = LlenarProducto(producto, NombreUsuario);
                Console.WriteLine("Guardando información...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Add(producto);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("Producto creado éxitosamente");
                    MenuProductos(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                CrearProducto(NombreUsuario);
            }
        }

        public static Producto LlenarProducto(Producto producto, string NombreUsuario)
        {
            Console.Write("Nombre: ");
            producto.Nombre = Console.ReadLine();
            Console.Write("Descripción: ");
            producto.Descripcion = Console.ReadLine();
            Console.Write("Precio: ");
            producto.Precio = decimal.Parse(Console.ReadLine());
            Console.Write("Costo: ");
            producto.Costo = decimal.Parse(Console.ReadLine());
            Console.Write("Cantidad: ");
            producto.Cantidad = decimal.Parse(Console.ReadLine());
            Console.Write("Tamaño: ");
            producto.Tamano = Console.ReadLine();

            return producto;
        }

        public static void BuscarProductos(string NombreUsuario)
        {
            Console.WriteLine("Buscar productos");
            Console.WriteLine("------------------------------------------------------------------------------");
            Console.Write("Ingrese el nombre del producto o presione 'Enter' para mostrar todos los registros: ");
            string buscar = Console.ReadLine();
            Console.WriteLine("Buscando...");
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    IQueryable<Producto> productos = context.Productos.Where(p => p.Nombre.Contains(buscar));
                    if (productos.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (Producto producto in productos)
                        {
                            Console.WriteLine("Producto: " + producto.Nombre + ", Precio: " + producto.Precio + " MXN, Descripción: " + producto.Descripcion);
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No se encontraron registros...");
                    }
                    Console.WriteLine("------------------------------------------------------------------------------");
                    MenuProductos(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuProductos(NombreUsuario);
            }
        }

        public static void EditarProducto(string NombreUsuario)
        {
            try
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    var listaproductos = context.Productos.ToList();
                    if (listaproductos.Any())
                    {
                        Console.Clear();
                        Console.WriteLine("Lista de registros");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        foreach (var item in listaproductos)
                        {
                            Console.WriteLine("Código del producto: " + item.Id + ", Producto: " + item.Nombre + ", Precio: " + item.Precio + " MXN, Descripción: " + item.Descripcion);
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("No se encontraron registros...");
                        Console.WriteLine("------------------------------------------------------------------------------");
                        MenuProductos(NombreUsuario);
                    }
                }
                Console.WriteLine("Editar producto");
                Console.WriteLine("------------------------------------------------------------------------------");
                Producto producto = SelecionarProducto();
                producto = LlenarProducto(producto, NombreUsuario);
                Console.WriteLine("Actualizando registro...");
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    context.Update(producto);
                    context.SaveChanges();
                    Console.Clear();
                    Console.WriteLine("producto actualizado éxitosamente");
                    MenuProductos(NombreUsuario);
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                MenuProductos(NombreUsuario);
            }
        }

        public static Producto SelecionarProducto()
        {
            Console.Write("Seleciona el código de producto: ");
            try
            {
                int Id = int.Parse(Console.ReadLine());
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    Producto producto = context.Productos.Find(Id);
                    if (producto == null)
                    {
                        SelecionarProducto();
                    }
                    return producto;
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine("Hubo un error, comuniquese con el administrador. Error : " + e.Message);
                Producto producto = new Producto();
                return producto;
            }
        }

        public static void EliminarProducto(string NombreUsuario)
        {
            Console.WriteLine("Eliminar detalle");
            Console.WriteLine("------------------------------------------------------------------------------");
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                var listaproductos = context.Productos.ToList();
                if (listaproductos.Any())
                {
                    Console.Clear();
                    Console.WriteLine("Lista de registros");
                    Console.WriteLine("------------------------------------------------------------------------------");
                    foreach (var item in listaproductos)
                    {
                        Console.WriteLine("Código del producto: " + item.Id + ", Producto: " + item.Nombre + ", Precio: " + item.Precio + " MXN, Descripción: " + item.Descripcion);
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("No se encontraron registros...");
                    Console.WriteLine("------------------------------------------------------------------------------");
                    MenuProductos(NombreUsuario);
                }
                Console.WriteLine("------------------------------------------------------------------------------");
            }
            Producto producto = SelecionarProducto();
            Console.WriteLine("Eliminando registro...");
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                context.Remove(producto);
                context.SaveChanges();
                Console.Clear();
                Console.WriteLine("Producto eliminado éxitosamente");
                MenuProductos(NombreUsuario);
            }
        }
        #endregion 
    }
}

