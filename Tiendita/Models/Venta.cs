﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiendita.Models
{
    public class Venta
    {
        public int Id { get; set; }
        public decimal Total { get; set; }
        public DateTime Fecha { get; set; }
        public string Cliente { get; set; }

        //Relaciones
        public virtual ICollection<Detalle> Detalles { get; set; }
    }
}
