﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tiendita.Models
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            FechaDeAlta = DateTime.Now;
        }
        public DateTime FechaDeAlta { get; set; }

        public virtual Usuario Usuario { get; set; }
    }

}
